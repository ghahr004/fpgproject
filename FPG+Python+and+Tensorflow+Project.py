import pandas as pd
from sklearn.metrics import accuracy_score
from sklearn.preprocessing import LabelEncoder
from sklearn.linear_model import LogisticRegression
from sklearn.cross_validation import train_test_split
from sklearn.linear_model import SGDClassifier
from sklearn.ensemble import RandomForestClassifier
import numpy as np
from sklearn.neural_network import MLPClassifier
from pprint import pprint
from sklearn.neighbors import KNeighborsClassifier
from sklearn.multiclass import OneVsOneClassifier
from sklearn.model_selection import cross_val_predict
from sklearn.metrics import confusion_matrix
import matplotlib
import matplotlib.pyplot as plt
from sklearn.model_selection import cross_val_score
from sklearn import tree

# The data is located in the following url in my bitbucket repository.

url = 'https://bitbucket.org/ghahr004/fpgproject/raw/84e55fed85c35fe91e61b44b04e367a22cd2444c/Datacsv/Data%20Set.csv'

# Using pandas, we put the data in a dataframe.

#FPG = pd.read_csv(url,index_col=0,parse_dates=[0])


FPG = pd.read_csv(url)
FPG.head()


# The head gives the first 5 rows of the dataset. As is it shows, we have 4 non-integer columns.
# Since the materials in these columns are repeatative strings, we transform these numbers into
# integers using the built-in function LabelEncoder() in Python.

encoder = LabelEncoder()

Hotel_Name = FPG['Hotel Name']
Observation_Category = FPG['Observation Category']
Performance_Leader = FPG['Performance Leader(Coach)']
Recommended_Coaching = FPG['Recommended Coaching']


# Since from this step on we will deal with the columns 'Hotel Name', 'Observation Category',
# 'Performance Leader(Coach)' and 'Recommended Coaching' as numbers, we save the names
# that correspond to the numbers as follows.

Hotel_Name_encoded = encoder.fit_transform(Hotel_Name)

Hotel_Name_string = encoder.classes_

print Hotel_Name_string

Observation_Category_encoded = encoder.fit_transform(Observation_Category)

Observation_Category_string = encoder.classes_

Performance_Leader_encoded = encoder.fit_transform(Performance_Leader)

# The strings that correspond to numbers are:

Performance_Leader_string = encoder.classes_

Recommended_Coaching_encoded = encoder.fit_transform(Recommended_Coaching)

Recommended_Coaching_string = encoder.classes_

print Recommended_Coaching_string[7]
print Recommended_Coaching_string[11]

# Principal Positive Attitiude
# Top-Down Selling

# We will make a new table of our data where there is no
# strings and strings in FPG are all encoded to integers.

FPG_modified = FPG

# In the new dataFrame FPG_modified, we change and modify our dataset into integers using 
# the columns produced above.

FPG_modified['Hotel Name'] = Hotel_Name_encoded
FPG_modified['Observation Category'] = Observation_Category_encoded
FPG_modified['Performance Leader(Coach)'] = Performance_Leader_encoded
FPG_modified['Recommended Coaching'] = Recommended_Coaching_encoded

FPG_modified.head()


# Now it is time to set every column except Recommended Coaching which is the target
# in FPG_modified for X and set y to be the target which is Recommended Coaching.

X = FPG_modified.drop(['Recommended Coaching'], axis = 1)

y =  FPG_modified['Recommended Coaching']


# We shuffle and set 80 percent of data in the training set and set 20 percent of the data in
# our test set.

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

dict_of_accuracy_of_classifiers={}


# As the target varies into integers from 1 to 12, Softmax regression looks very suitable.

softmax_reg = LogisticRegression(multi_class="multinomial", solver="lbfgs", C=1.0)

softmax_reg.fit(X_train,y_train)

y_predict_softmax_reg = softmax_reg.predict(X_test)

acc_softmax = accuracy_score(y_test, y_predict_softmax_reg)

dict_of_accuracy_of_classifiers['ACCURACY of SOFTMAX CLASSIFIER'] = acc_softmax


# The accuracy of softmax classifier is 49% which is not very good.
# We look closely on the prediction of softmax regression
# using its confusion matrix. We have

y_train_pred_softmax = cross_val_predict(softmax_reg, X_train, y_train, cv = 3)
conf_mx = confusion_matrix(y_train, y_train_pred_softmax)
print conf_mx

# Having some visualization of the matrix we will have an understanding that where it has done
# well and where it has performed badly.

plt.matshow(conf_mx, cmap = plt.cm.gray)
plt.show()


# The above matrix tells us that, the softmax regression has done well in predictiong 8 to be 8
# and 12 to be 12 but on the rest of predictions, it has done very poorly.
# Next time, we will use Random Forest Classifier.

forest_clf = RandomForestClassifier()
forest_clf.fit(X_train, y_train)
y_predict_RandomForest = forest_clf.predict(X_test)

acc_RandomForest = accuracy_score(y_test, y_predict_RandomForest)
    
dict_of_accuracy_of_classifiers['ACCURACY of Random Forest Classifier'] = acc_RandomForest

# The accuracy is 94% which is very good. Lets look at the confusin matrix.

y_train_pred_forest_clf = cross_val_predict(forest_clf, X_train, y_train, cv = 3)
conf_mx = confusion_matrix(y_train, y_train_pred_forest_clf )
print conf_mx

plt.matshow(conf_mx, cmap = plt.cm.gray)
plt.show()


# As the above square tells us, the model, has done great overall with a little misprediciton on predicting 1 to be 1
# other than that it has done great!

# We do the same observation on linear SVM, quadratic SVM, and cubic SVM.

from sklearn import svm
linear_svm_clf = svm.SVC(gamma=0.001, C=1)
linear_svm_clf.fit(X_train, y_train)
y_predict_linearsvm = linear_svm_clf.predict(X_test)

acc_linearsvm = accuracy_score(y_test, y_predict_linearsvm)

dict_of_accuracy_of_classifiers['ACCURACY of Linear SVM'] = acc_linearsvm

y_train_pred_linear_svm_clf = cross_val_predict(linear_svm_clf, X_train, y_train, cv = 3)
conf_mx = confusion_matrix(y_train, y_train_pred_linear_svm_clf )
print conf_mx

plt.matshow(conf_mx, cmap = plt.cm.gray)
plt.show()


quadratic_svm_clf = svm.SVC(gamma=0.001, C=2)
quadratic_svm_clf.fit(X_train, y_train)
y_predict_quadraticsvm = quadratic_svm_clf.predict(X_test)

acc_quadratic_svm_clf = accuracy_score(y_test, y_predict_quadraticsvm)

dict_of_accuracy_of_classifiers['ACCURACY OF quadratic SVM'] = acc_quadratic_svm_clf


y_train_pred_quadratic_svm_clf = cross_val_predict(quadratic_svm_clf, X_train, y_train, cv = 3)
conf_mx = confusion_matrix(y_train, y_train_pred_quadratic_svm_clf )
print conf_mx


plt.matshow(conf_mx, cmap = plt.cm.gray)
plt.show()

cubic_svm_clf = svm.SVC(gamma=0.001, C=3)
cubic_svm_clf.fit(X_train, y_train)
y_predict_cubicsvm = cubic_svm_clf.predict(X_test)

acc_qubic_svm_clf = accuracy_score(y_test, y_predict_cubicsvm)

dict_of_accuracy_of_classifiers['ACCURACY of CUBIC SVM'] = acc_qubic_svm_clf


y_train_pred_cubic_svm_clf = cross_val_predict(cubic_svm_clf, X_train, y_train, cv = 3)
conf_mx = confusion_matrix(y_train, y_train_pred_cubic_svm_clf )
print conf_mx

plt.matshow(conf_mx, cmap = plt.cm.gray)
plt.show()


# As the graphs show, the SVMs do almost the same. They predict 8 to be 8 well but on the rest of the
# predicitons they do poorly.


mlpclassifier = MLPClassifier(solver='lbfgs', alpha=1e-5,hidden_layer_sizes=(5, 2), random_state=1)
mlpclassifier.fit(X_train, y_train)
y_predict_mlpclassifier = mlpclassifier.predict(X_test)

acc_mlpclassifier = accuracy_score(y_test, y_predict_mlpclassifier)

dict_of_accuracy_of_classifiers['ACCURACY of mlpclassifier'] = acc_mlpclassifier


y_train_pred_mlpclassifier = cross_val_predict(mlpclassifier, X_train, y_train, cv = 3)
conf_mx = confusion_matrix(y_train, y_train_pred_mlpclassifier)
print conf_mx


plt.matshow(conf_mx, cmap = plt.cm.gray)
plt.show()


# The  Multi-layer Perceptron model does not do well at all.

clf_tree = tree.DecisionTreeClassifier()
clf_tree.fit(X_train, y_train)
y_predict_clf_tree = clf_tree.predict(X_test)

acc_clf_tree = accuracy_score(y_test, y_predict_clf_tree)

dict_of_accuracy_of_classifiers['ACCURACY of Decision Tree'] = acc_clf_tree


y_train_pred_clf_tree = cross_val_predict(clf_tree, X_train, y_train, cv = 3)
conf_mx = confusion_matrix(y_train, y_train_pred_clf_tree)
print conf_mx

plt.matshow(conf_mx, cmap = plt.cm.gray)
plt.show()


# As it shows,the decision tree classifier is great! 100 percent accuracy!

knn = KNeighborsClassifier(n_neighbors=12)
knn.fit(X_train, y_train)
y_predict_knn = knn.predict(X_test)

acc_knn = accuracy_score(y_test, y_predict_knn)
    
dict_of_accuracy_of_classifiers['ACCURACY OF  K nearest neighbors'] = acc_knn


y_train_pred_knn = cross_val_predict(knn, X_train, y_train, cv = 3)
conf_mx = confusion_matrix(y_train, y_train_pred_knn)
print conf_mx

plt.matshow(conf_mx, cmap = plt.cm.gray)
plt.show()

sgd_clf = SGDClassifier()
sgd_clf.fit(X_train, y_train)
y_predict_sgd_clf= sgd_clf.predict(X_test)

acc_sgd_clf = accuracy_score(y_test, y_predict_sgd_clf)

dict_of_accuracy_of_classifiers['ACCURACY of SGDClassifier'] = acc_sgd_clf

y_train_pred_sgd_clf = cross_val_predict(sgd_clf, X_train, y_train, cv = 3)
conf_mx = confusion_matrix(y_train, y_train_pred_sgd_clf)
print conf_mx

plt.matshow(conf_mx, cmap = plt.cm.gray)
plt.show()


# The following is the overall performance of classifiers we used in this classification project.


sorted(dict_of_accuracy_of_classifiers.items(), key=lambda x:x[1])


# Having examined some classication models above, lets try binary models. Sometimes
# it is important for us that a classifier has done well on a particular member of the
# target (Recommended Coaching). 

from sklearn.model_selection import cross_val_score


y_train_8 = (y_train == 7)

cross_val_score(sgd_clf, X_train, y_train_8, cv = 3, scoring = "accuracy")

# Training a binary Classifier


# Let's simplify the classification problem and only try to identify one of the members of the target (Recommended Coaching). 
y_train_8 = (y_train == 7)
y_test_8 = (y_test == 7)


# Note that the elements in the target are encoded to be numbers from 0 to 11.
# Therefore, the 8th encoded member of the target is number 7. Which is Principle Positive Attitude. For now, we pick Decision Tree Classifier.
clf_tree.fit(X_train, y_train_8)

some_data = X.loc[1000]

clf_tree.predict([some_data])

y.loc[1000]


# The Decision Tree Classifer has predicted that the 1000th instance should not be labeled 8 and it is verified that the 1000th instance is a 4.
# We can set up the confusion matrix for the above binary classification model to evaluate its performance.



from sklearn.model_selection import cross_val_predict

# Cross val predict performs K-fold cross validation it returs predicitons on each test fold.

y_train_pred = cross_val_predict(clf_tree, X_train, y_train_8, cv = 3)


# Let's observe the behaviour of our binary model in the confusion matrix.


from sklearn.metrics import confusion_matrix

confusion_matrix(y_train_8, y_train_pred)


# Each row in the confusion matrix represents an actual class, while each column represents predicted class. The first row
# determines non-8s (the negative class): 1833 of them are classified correctly while 5 of which are classified wrongly.
# The first row are TN (True Negative) and FP (False Positive) classes respectively. The Second row considers the 8th class. 9 of 8th class
# are classified to be non 8 (Flase Negative) and 661 of them are classified to be 8 (True positive).
# The precision of the above binary classifier is defined by TP/(TP + FP) where TP is the the true positive
# and FP is the flase positives. The dual of precision is recall is which defined by recall = TP/(TP + FN)
# Where TP is the true positives while FN is the false negatives.

# Let's calculate the precision and recall of the above model.

from sklearn.metrics import precision_score, recall_score

precision_score(y_train_8, y_train_pred)

recall_score(y_train_8, y_train_pred)


# Based on the above calculations, our classifier, when claims an instance is 8 is true 98 percent of the times!
# and it detects 98 percent of the instances that are labeled to be 8!
# The F1 score, combines the percision and recall. F1 is the harmonic mean of precision and recall. 

from sklearn.metrics import f1_score

f1_score(y_train_8, y_train_pred)


# F1 classifier favors classifiers that have similar precision and recall as it is the case in using Decision
# Tree Classifier.

# Precision/Recall relations

# In the next set of materials, before we start to use Tensorflow to do our project,
# we explore the relation between Precion/ Recall and will see how this relation
# will help us to determine which classifier to use when we are dealing with a binary
# classifier as we are using a binary classifier now.

# Lets use Stochastic Gradient Descent and determine how precision and recall works
# when the binary model is based on if the label is equal to 8th element of the label's list which is Princile Positive Attitude.

y_scores = cross_val_predict(sgd_clf, X_train, y_train_8, cv = 5, method = "decision_function")

y_scores.shape


y_scoress = [x[1] for x in y_scores]

from sklearn.metrics import precision_recall_curve

X_train.shape


y_train.shape

precisions, recalls, thresholds = precision_recall_curve(y_train_8, y_scoress)

def plot_precision_recall_vs_threshold(precisions, recalls, thresholds):
    plt.plot(thresholds, precisions[:-1], "b--", label = "Precision")
    plt.plot(thresholds, recalls[:-1], "g-", label = "Recall")
    plt.xlabel("Threshold")
    plt.legend(loc = "upper left")
    plt.ylim([0,1])


plot_precision_recall_vs_threshold(precisions, recalls, thresholds)
plt.show()

# Now we can select the threshold value that
# gives us the best presision/recall tradeoff.

def plot_precision_vs_recall(precisions, recalls):
    plt.plot(recalls[:-1], precisions[:-1], "b--", label = "Precision")
    #plt.plot(thresholds, recalls[:-1], "g-", label = "Recall")
    plt.xlabel("Recall")
    plt.legend(loc = "upper left")
    plt.ylim([0,1])

plot_precision_vs_recall(precisions, recalls)
plt.show()


# Clearly, Percision falls relatively sharpy if recall is larger than 50 percent. Depending on the aim of the project
# we may want to select precision/recall tradeoff before that drop. 

# The ROC Curve

# The receiver operating characteristic (ROC) curve is another common tool used with binary clasifiers. The ROC curve
# plots the true positive rate (another name for recall) agaist false positive rate. 

from sklearn.metrics import roc_curve

fpr, tpr, thresholds = roc_curve(y_train_8, y_scoress)

def plot_roc_curve(fpr, tpr, label = None):
    plt.plot(fpr, tpr, linewidth = 2, label = label)
    plt.plot([0,1], [0,1], 'k--')
    plt.axis([0,1,0,1])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')

plot_roc_curve(fpr, tpr)
plt.show()


# One crucial way to determine the performance of a binary classifier is to determine the area under the graph
# on the ROC curve. The more the area is close to one, the more it is reliable. 

# The following module will claculate the area under the graph for the binary classifier of 
# Stochastic Gradient Descent.

from sklearn.metrics import roc_auc_score

roc_auc_score(y_train_8, y_scoress)


# Lets assume that we want to compare SGDClassifier with Random Forest binary classifier. ROC curve can help us.
# The more the ROC of the corresponding classifier is away from the identity function (the dotted line), the
# better a classifier is.

from sklearn.ensemble import RandomForestClassifier

forest_clf = RandomForestClassifier(random_state = 42)

y_probas_forest = cross_val_predict(forest_clf, X_train, y_train_8, cv = 3, method = "predict_proba")

y_scores_forest = y_probas_forest[:, 1]

fpr_forest, tpr_forest, thresholds_forest = roc_curve(y_train_8, y_scores_forest)

plt.plot(fpr, tpr, label = "Decision Tree Classifier")
plot_roc_curve(fpr_forest, tpr_forest, "Random Forest")
plt.legend(loc = "lower right")
plt.show()

roc_auc_score(y_train_8, y_scores_forest)


# Since the Decision Tree Classifer is more away from the identity function in the above graph, it perforams
# a better binary classifier.

# Solving the problem using tensorflow

# In the next set of materials, we try to do the problem with the powerful ML library, Tensorfolw. 

# First, we import tensorflow.
import tensorflow as tf

FPG_modified.head()


# Since the titles in the above table are named with space such as Hotel Name and ... we normalize the names
# with setting an underline between the words in the title of columns.

FPG_modified = FPG_modified.rename(columns={'Hotel Name': 'Hotel_Name', 'Observation Category': 'Observation_Category', 'Performance Leader(Coach)': 'Performance_Leader', 'Observation Time in Seconds':'Observation_Time_in_Seconds', 'Recommended Coaching': 'Recommended_Coaching'})

FPG_modified.columns


# We need to normalize the numbers in each column. We normalize by multiplying deducting the minimum of
# each column from each element in the column and then dividing the column by maximum - minimum.

cols_to_normalize = [ u'Hotel_Name',        u'Observation_Category',
                u'Performance_Leader',                       u'Score',
       u'Observation_Time_in_Seconds']

FPG_modified[cols_to_normalize] = FPG_modified[cols_to_normalize].apply(lambda x: (x-x.min())/(x.max() - x.min()))

FPG_modified.head()


# Now the columns (except the target (Recommended_Coaching)) are normalized. each member of each column is 
# a number between 0 to 1.

Hotel_Name = tf.feature_column.numeric_column('Hotel_Name')
Observation_Category = tf.feature_column.numeric_column('Observation_Category')
Performance_Leader = tf.feature_column.numeric_column('Performance_Leader')
Score = tf.feature_column.numeric_column('Score')
Observation_time_in_seconds = tf.feature_column.numeric_column('Observation_Time_in_Seconds')

x_data = FPG_modified.drop('Recommended_Coaching', axis = 1)


# In the x component of our data which we denote by x_data, we take everything in FPG_modified except
# the labels which is the recommended 

x_data.head()

labels = FPG_modified['Recommended_Coaching']

type(labels)


# We split the data into train and test by giving 70 percent for training and 30 percent fro testing.

from sklearn.model_selection import train_test_split

X_train, X_test, y_train, y_test = train_test_split(x_data, labels, test_size = 0.3, random_state = 42)


# We define our input function as follows.

input_func = tf.estimator.inputs.pandas_input_fn(x = X_train, y=y_train, batch_size=10, num_epochs=1000, shuffle=True)

feat_cols = [Hotel_Name, Observation_Category,Performance_Leader , Score,Observation_time_in_seconds]


# We choose our model to be Linear Classifier. We take the featur_columns to be feat_cols as above and we take
# number of classes to be 12 as the labels are intergers from 1 to 12.

model = tf.estimator.LinearClassifier(feature_columns=feat_cols, n_classes=12)

# Now it is time to train our model.

model.train(input_fn=input_func, steps = 1000)


# Lets evaluate our model.

eval_input_func = tf.estimator.inputs.pandas_input_fn(x=X_test, y=y_test, batch_size=10, num_epochs=1, shuffle=False)

results = model.evaluate(eval_input_func)


print results


# The accuracy is only 53 percent which is not very good.

pred_input_func = tf.estimator.inputs.pandas_input_fn(x = X_test, batch_size=10, num_epochs=1, shuffle=False)

predictions = model.predict(pred_input_func)

my_pred = list(predictions)

# Let's change our model and use Densely Neural Network (DNN) Classifier as a new model. We take the hidden 
# layers to be consisted of 10 nodes each and take the total hidden layers to be 3.

dnn_model = tf.estimator.DNNClassifier(hidden_units=[10,10,10], feature_columns=feat_cols, n_classes=12)

input_func = tf.estimator.inputs.pandas_input_fn(X_train, y_train,batch_size=10, num_epochs=1000, shuffle=True)

dnn_model = tf.estimator.DNNClassifier(hidden_units=[10,10,10], feature_columns=feat_cols, n_classes=12)

dnn_model.train(input_fn=input_func, steps=1000)

eval_input_func = tf.estimator.inputs.pandas_input_fn(x=X_test, y=y_test, batch_size=10,num_epochs=1,shuffle=False)

dnn_model.evaluate(eval_input_func)


# The DNNClassifier also, does not seem to be fitting! It is underfitting. Next time, we will use softmax regression
# and then optimize it using Convolutional Neural Networks. We will see that it will help us a lot to make the model
# work better.

# First, we need to convert our data into arrays of arrays as follows.


FPG_data_array = FPG_modified.as_matrix(columns=None)
FPG_data_target = labels.as_matrix(columns=None)

type(FPG_data_array[0])


FPG_data_target[0]


# Our main aim is to use the ideas used in the tensorflow hello world problem in which 
# a set of 5000 arrays of lengths 784 are used. Each array represents a number between 0 to 9.
# Our task is to set these 6 features in a arrays of lengths 784 and then perform softmax regression and 
# then imporove our results using Convolutional Neural Netwok.

def image_builder(v):
    t = np.zeros([784])
    for i in range(len(v)):
        t[390 + i] = v[i]
    return t    

def array_of_images(array_of_data):
    s = list(array_of_data)
    t = []
    for a in s:
        t.append(image_builder(a))
    return np.asarray(t)

FPG_images = array_of_images(FPG_data_array)

x = tf.placeholder(tf.float32, shape = [None, 784])

W = tf.Variable(tf.zeros([784, 12]))
b = tf.Variable(tf.zeros([12]))

y = tf.matmul(x,W) + b

y_true = tf.placeholder(tf.float32, shape=[None, 12])

cross_entropy = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(labels=y_true, logits=y))

optimizer = tf.train.GradientDescentOptimizer(learning_rate = 0.5)
train = optimizer.minimize(cross_entropy)

X_train, X_test, y_train, y_test = train_test_split(FPG_images, FPG_data_target, test_size=0.2, random_state=42)

def y_batch_builder(y_target):
    s = np.array([0,0,0,0,0,0,0,0,0,0,0,0])
    s[y_target] = 1
    return s

def y_batch_array(r):
    l = list(r)
    t = []
    for i in l:
        t.append(y_batch_builder(i))
    return np.asarray(t)    

init = tf.global_variables_initializer()

from sklearn.utils import shuffle

with tf.Session() as sess:
    sess.run(init)
    
    for step in range(1000):
        batch_x = shuffle(X_train, y_train, random_state=0)[0][:100]
        batch_y = shuffle(X_train, y_train, random_state=0)[1][:100]
        q = y_batch_array(batch_y)
        #print batch_x.shape
        #print q.shape
        sess.run(train, feed_dict = {x:batch_x, y_true:q})
        
        
 # Time to evaluate the model.

    correct_prediction = tf.equal(tf.arg_max(y,1), tf.arg_max(y_true,1))
    
 #[True, False, True, ...]   ------> [1,0,1]
    acc = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))
    
    # Predicted [3,4] True [3,9]
    # [True, False]
    # [1.0, 0.0]
    #0.5
    print(sess.run(acc, feed_dict={x:X_test, y_true:y_batch_array(y_test)}))


# The accuracy of softmax regression performed on Desely Connected Neural Network (DNN) is very low but there is
# a way to make it better. We will perform Convolutional Neural Network and modify our results.

def init_weights(shape):
    init_random_dist = tf.truncated_normal(shape, stddev = 0.1)
    return tf.Variable(init_random_dist)


def init_bias(shape):
    init_bias_vals = tf.constant(0.1, shape=shape)
    return tf.Variable(init_bias_vals)


def Conv2d(xx,W):
    #x -- > [batch, H, W, Channels]
    # W --> [filter H, filter W, Channels IN, Channels Out]
    return tf.nn.conv2d(xx,W, strides=[1,1,1,1], padding='SAME')

def max_pool_2by2(xx):
    # x --> [batch, h, w, c]
    return tf.nn.max_pool(xx, ksize=[1,2,2,1], strides=[1,2,2,1], padding='SAME')


def convolutional_layer(input_x, shape):
    W = init_weights(shape)
    b = init_bias([shape[3]])
    return tf.nn.relu(Conv2d(input_x, W)+b)

def normal_full_layer(input_layer, size):
    input_size = int(input_layer.get_shape()[1])
    W = init_weights([input_size, size])
    b = init_bias([size])
    return tf.matmul(input_layer, W) + b


x_image = tf.reshape(x, [-1, 28, 28, 1])

convo_1 = convolutional_layer(x_image, shape=[5,5,1,32])
convo_1_pooling = max_pool_2by2(convo_1)

convo_2 = convolutional_layer(convo_1_pooling, shape=[5,5,32,64])
convo_2_pooling = max_pool_2by2(convo_2)

convo_2_flat = tf.reshape(convo_2_pooling, [-1,7*7*64])
full_layer_one = tf.nn.relu(normal_full_layer(convo_2_flat, 1024))

hold_prob = tf.placeholder(tf.float32)
full_one_dropout = tf.nn.dropout(full_layer_one, keep_prob=hold_prob)

y_pred = normal_full_layer(full_one_dropout, 12)


cross_entropy = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(labels=y_true, logits=y_pred))

optimizer = tf.train.AdamOptimizer(learning_rate=0.001)
train = optimizer.minimize(cross_entropy)

init = tf.global_variables_initializer()

steps = 5000

with tf.Session() as sess:
    sess.run(init)
    for i in range(steps):
        
        batch_x = shuffle(X_train, y_train, random_state=0)[0][:100]
        batch_y = shuffle(X_train, y_train, random_state=0)[1][:100]
        q = y_batch_array(batch_y)
        #print batch_x.shape
        #print q.shape
        #sess.run(trainn, feed_dict = {xx:batch_x, y_truee:q})
        
        
        
        #batch_x, batch_y = mnist.train.next_batch(50)
        
        sess.run(train, feed_dict={x:batch_x, y_true: q, hold_prob: 0.5})
        
        if i%100 == 0:
            
            print "ONE STEP: {}".format(i)
            
            print("ACCURACY: ")
            
            matches = tf.equal(tf.arg_max(y_pred,1), tf.arg_max(y_true,1))
            
            acc = tf.reduce_mean(tf.cast(matches, tf.float32))
            
            
            print sess.run(acc, feed_dict={x: X_test, y_true: y_batch_array(y_test), hold_prob: 1.0})
            
            print('\n')


# Now the accuracy of Softmax Regression is over 97 percent using CNN (Convolutional Neural Network.)


