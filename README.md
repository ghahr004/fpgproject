# README #

The following is a project I did for my job application for a private company. 

The data is consisted of over 3000 rows which have 6 features and there is a target called Recommended Coaching. 

I first used Python's regular MLlibraries for the classifications and then
did binary classifcations and copared SGDClassifier with Decision Tree Clasifier using 
ROCCurve and then in the second part of the project I used Tensorflow. In the Tensorflow part, 
first I used LinearClassifier and then DNNClassifer for my classification but they did not lead into a good result then
I used Softmax Regression using DNN which also did not lead into a good result but I could improve my Softmax Regression
as high as 98 percent accuracy using Densely Neural Network. 

In the branch FPG_Spark, I used RandomForestClassifier and DecisionTreeClassifier as Machine Learning libraries in
Spark as models for classification.



